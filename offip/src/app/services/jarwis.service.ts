import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JarwisService {

  private baseUrl = 'https://offip.co.za/Backend/api';

  constructor(
    private http: HttpClient,
  ) { }

  loggedIn() {
    return localStorage.getItem('token');
  }

  login(data: any) {
    return this.http.post(`${this.baseUrl}/login.php`, data);
  }

  getFooter() {
    return this.http.get(`${this.baseUrl}/getLooter.php`);
  }

  getLogo() {
    return this.http.get(`${this.baseUrl}/getLogo.php`);
  }

  createSurvey(data: any) {
    return this.http.post(`${this.baseUrl}/createSurvey.php`, data);
  }

  getSurvey(data: any) {
    return this.http.post(`${this.baseUrl}/getSurvey.php`, data);
  }

  addSurveyQuestion(data: any) {
    return this.http.post(`${this.baseUrl}/addSurveyQuestion.php`, data);
  }

  getSurveyQuestion(data: any) {
    return this.http.post(`${this.baseUrl}/getSurveyQuestion.php`, data);
  }

  getSurveyLastQuestion(data: any) {
    return this.http.post(`${this.baseUrl}/getSurveyLastQuestion.php`, data);
  }

  inviteUserToSurvey(data: any) {
    return this.http.post(`${this.baseUrl}/inviteUserToSurvey.php`, data);
  }

  process(data: any) {
    return this.http.post(`${this.baseUrl}/process.php`, data);
  }

  getQuestion(data: any) {
    return this.http.post(`${this.baseUrl}/getQuestion.php`, data);
  }

  contact(data: any) {
    return this.http.post(`${this.baseUrl}/contact.php`, data);
  }

  account(data: any) {
    return this.http.post(`${this.baseUrl}/account.php`, data);
  }

  getAccount(data: any) {
    return this.http.post(`${this.baseUrl}/getAccount.php`, data);
  }

  profilePic(data: any) {
    return this.http.post(`${this.baseUrl}/profilePic.php`, data);
  }

  deleteQuestion(data: any) {
    return this.http.post(`${this.baseUrl}/deleteQuestion.php`, data)
  }

  editQuestion(data: any) {
    return this.http.post(`${this.baseUrl}/editQuestion.php`, data)
  }

  deleteSurvey(data: any) {
    return this.http.post(`${this.baseUrl}/deleteSurvey.php`, data)
  }

  upload(data: any): Observable<HttpEvent<any>> {
    const req = new HttpRequest('POST', `${this.baseUrl}/userUpload.php`, data, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }

  getImage(data: any) {
    return this.http.post(`${this.baseUrl}/getUserImage.php`, data)
  }

  getReports(data: any) {
    return this.http.post(`${this.baseUrl}/getReportsByCreator.php`, data)
  }

  deleteReport(data: any) {
    return this.http.post(`${this.baseUrl}/deleteReport.php`, data)
  }

}

