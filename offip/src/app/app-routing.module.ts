import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './auth/login/login.component';
import { ContactComponent } from './contact/contact.component';
import { IndexComponent } from './index/index.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { AccountComponent } from './pages/account/account.component';
import { DraftComponent } from './pages/draft/draft.component';
import { HomeComponent } from './pages/home/home.component';
import { SurveyModalComponent } from './pages/survey-modal/survey-modal.component';
import { SurveyComponent } from './pages/survey/survey.component';
import { TakeSurveyComponent } from './pages/take-survey/take-survey.component';
import { TermsComponent } from './terms/terms.component';

const routes: Routes = [

  {
    path: '',
    component: IndexComponent,
  },

  {
    path: 'login',
    component: LoginComponent,
  },

  {
    path: 'contact',
    component: ContactComponent,
  },

  {
    path: 'about',
    component: AboutComponent,
  },

  {
    path: 'home',
    component: HomeComponent,
  },

  {
    path: 'survey',
    component: SurveyComponent,
  },

  {
    path: 'survey/:name',
    component: SurveyModalComponent,
  },

  {
    path: 'draft',
    component: DraftComponent,
  },

  {
    path: 'account',
    component: AccountComponent,
  },

  {
    path: 'take-survey/:name/:id',
    component: TakeSurveyComponent,
  },

  {
    path: 'terms',
    component: TermsComponent,
  },

  {
    path: 'notfond',
    component: NotfoundComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
