import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  public loggedIn: boolean = false;
  public users: any;
  public token: any;
  public surveys: any;
  public username = '';
  public error = null;
  public success = null;
  public pic: any | undefined;
  public profile: any;

  public form = {
    name: '',
    description: ''
  }

  constructor(
    private Jarwis: JarwisService,
    private router: Router,
    private Token: TokenService,
    private Auth: AuthService,
  ) { this.router.routeReuseStrategy.shouldReuseRoute = () => false; }

  onSubmit() {
    this.Auth.authStatus.subscribe(value => this.loggedIn = value);
    this.token = localStorage.getItem('token');
    this.users = this.Token.payload(this.token);
    this.username = this.users.data.name;

    let myFormData = new FormData();
    myFormData.append('name', this.form.name);
    myFormData.append('description', this.form.description);
    myFormData.append('creator', this.username);

    this.Jarwis.createSurvey(myFormData).subscribe(
      success => this.handleSuccess(success),
      error => this.handleError(error)
    );

  }

  deleteSurvey(survey: any) {
    this.Jarwis.deleteSurvey({ 'survey': survey }).subscribe((data) =>
      this.handleResponse(data)
    );
  }

  handleResponse(data: any) {
    this.ngOnInit();
  }

  openSurvey(name: string) {
    this.router.navigate(['/survey', name]);
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  ngOnInit() {

    this.Auth.authStatus.subscribe(value => this.loggedIn = value);
    this.token = localStorage.getItem('token');
    this.users = this.Token.payload(this.token);
    this.username = this.users.data.name;

    this.Jarwis.getSurvey({ 'creator': this.username }).subscribe((response: any) => {
      this.surveys = response['surveys'];
    })

    let myFormData = new FormData();
    myFormData.append('name', this.username);

    this.Jarwis.getAccount(myFormData).subscribe((response: any) => {
      this.profile = response['user'];
    });

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
    },
      error => this.handleError(error),
    );


  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
