import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-survey-modal',
  templateUrl: './survey-modal.component.html',
  styleUrls: ['./survey-modal.component.scss'],
})
export class SurveyModalComponent implements OnInit {

  public loggedIn: boolean = false;
  public surveyName: any;
  public error = '';
  public success = null;
  public questions: any | undefined;
  public question: any | undefined;
  public users: any;
  public token: any;
  public username = '';
  public pic: any | undefined;
  public profile: any;

  public form = {
    question: '',
    email: '',
    name: '',
    questionNo: '',
    editedQuestion: '',
  };

  constructor(
    private route: ActivatedRoute,
    private Jarwis: JarwisService,
    private router: Router,
    private Token: TokenService,
    private Auth: AuthService,
  ) { }

  //get survey name from url
  getName() {
    let surveyName = this.route.snapshot.paramMap.get('name');
    return surveyName;
  }

  onSubmit(number: any) {
    this.surveyName = this.getName();

    let myFormData = new FormData();
    myFormData.append('questionNo', number);
    myFormData.append('question', this.form.question);
    myFormData.append('survey', this.surveyName);

    this.Jarwis.addSurveyQuestion(myFormData).subscribe(
      (success) => this.handleSuccess(success),
      (error) => this.handleError(error)
    );
  }

  onEdit() {
    this.surveyName = this.getName();

    let myFormData = new FormData();
    myFormData.append('questionNo', this.form.questionNo);
    myFormData.append('question', this.form.editedQuestion);
    myFormData.append('survey', this.surveyName);

    this.Jarwis.editQuestion(myFormData).subscribe(
      (success) => this.handleSuccess(success),
      (error) => this.handleError(error)
    );
  }

  onInvite() {
    this.surveyName = this.getName();

    let myFormData = new FormData();
    myFormData.append('email', this.form.email);
    myFormData.append('name', this.form.name);
    myFormData.append('survey', this.surveyName);

    this.Jarwis.inviteUserToSurvey(myFormData).subscribe(
      (success) => this.handleSuccess(success),
      (error) => this.handleError(error)
    );
  }

  deleteQuestion(survey: any, questionNo: any) {
    this.Jarwis.deleteQuestion({ 'survey': survey, 'questionNo': questionNo }).subscribe((data) =>
      this.handleResponse(data)
    );
  }

  handleResponse(data: any) {
    this.ngOnInit();
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  ngOnInit() {
    this.surveyName = this.getName();

    this.Jarwis.getSurveyQuestion({ survey: this.surveyName }).subscribe((response: any) => {
      this.questions = response['questions'];
    },
      (error) => this.handleError(error)
    );

    this.Jarwis.getSurveyLastQuestion({ survey: this.surveyName }).subscribe((response: any) => {
      this.question = response['questions'];
    }
    );

    this.Auth.authStatus.subscribe(value => this.loggedIn = value);
    this.token = localStorage.getItem('token');
    this.users = this.Token.payload(this.token);
    this.username = this.users.data.name;

    let myFormData = new FormData();
    myFormData.append('name', this.username);

    this.Jarwis.getAccount(myFormData).subscribe((response: any) => {
      this.profile = response['user'];
    });

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
    },
      error => this.handleError(error),
    );

  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }
}
