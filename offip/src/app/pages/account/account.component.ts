import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';
import { HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  public form = {
    newName: '',
    age: '',
    code: '',
    province: '',
    country: '',
    town: '',
  }

  public users: any;
  public token: any;
  public username = '';
  public error: any;
  public success: any;
  public profile: any;
  public selectedFile: any = null;
  public pic: any | undefined;
  progress = 0;

  constructor(
    private Jarwis: JarwisService,
    private router: Router,
    private Token: TokenService,
  ) { }

  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  onSubmit() {

    this.token = localStorage.getItem('token');
    this.users = this.Token.payload(this.token);
    this.username = this.users.data.name;

    let myFormData = new FormData();
    myFormData.append('newName', this.form.newName);
    myFormData.append('age', this.form.age);
    myFormData.append('code', this.form.code);
    myFormData.append('province', this.form.province);
    myFormData.append('country', this.form.country);
    myFormData.append('town', this.form.town);
    myFormData.append('name', this.username);

    this.Jarwis.account(myFormData).subscribe(
      success => this.handleSuccess(success),
      error => this.handleError(error),
    );
  }

  onUpload() {

    this.token = localStorage.getItem('token');
    this.users = this.Token.payload(this.token);
    this.username = this.users.data.name;

    this.progress = 0;

    if (this.selectedFile === null) {
      this.error = "Please select an image.";
    }
    else {
      let myFormData = new FormData();
      myFormData.append('image', this.selectedFile, this.selectedFile.name);
      myFormData.append('name', this.username);

      this.Jarwis.upload(myFormData).subscribe(
        (event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          }
        },
        error => this.handleError(error)
      );
    }
  }


  ngOnInit() {

    this.token = localStorage.getItem('token');
    this.users = this.Token.payload(this.token);
    this.username = this.users.data.name;

    let myFormData = new FormData();
    myFormData.append('name', this.username);

    this.Jarwis.getAccount(myFormData).subscribe((response: any) => {
      this.profile = response['user'];
    });

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
    },
      error => this.handleError(error),
    );

  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
