import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JarwisService } from 'src/app/services/jarwis.service';
import { TokenService } from 'src/app/services/token.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public users: any;
  public token: any;
  public username = '';
  public error: any;
  public success: any;
  public profile: any;
  public selectedFile: any = null;
  public pic: any | undefined;
  public reports: any;

  constructor(
    private Jarwis: JarwisService,
    private router: Router,
    private Token: TokenService,
  ) { }

  handleResponse(data: any) {
    this.ngOnInit();
  }

  deleteReport(survey: any) {
    this.Jarwis.deleteReport({ 'survey': survey }).subscribe((data) =>
      this.handleResponse(data)
    );
  }

  //preview pdf then download it
  downloadReport(report: any) {
    console.log(report);
    window.open("https://offip.co.za/Backend" + report, "_blank");
  }

  ngOnInit() {
    this.token = localStorage.getItem('token');
    this.users = this.Token.payload(this.token);
    this.username = this.users.data.name;

    let myFormData = new FormData();
    myFormData.append('name', this.username);

    this.Jarwis.getImage(myFormData).subscribe((response: any) => {
      this.pic = response['Image'];
    },
      error => this.handleError(error),
    );

    this.Jarwis.getReports(myFormData).subscribe((response: any) => {
      this.reports = response['reports'];
    },
      error => this.handleError(error),
    );

  }

  handleError(error: any) {
    this.error = error.error.error;
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  logout(event: MouseEvent) {
    event.preventDefault();
    this.Token.remove();
    localStorage.clear();
    this.router.navigate(['']);
  }

}
