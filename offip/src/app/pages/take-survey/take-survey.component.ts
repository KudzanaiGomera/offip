import { Component, OnInit } from '@angular/core';
import { JarwisService } from 'src/app/services/jarwis.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-take-survey',
  templateUrl: './take-survey.component.html',
  styleUrls: ['./take-survey.component.scss']
})
export class TakeSurveyComponent implements OnInit {

  public surveyName: any;
  public next: any;
  public question: any | undefined;
  public qns: any;
  public questionNo = null;
  public error: any;
  public success: any;
  public done = '';

  public form = {
    choice: '',
    questionNum: ''
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private Jarwis: JarwisService,
  ) {  this.router.routeReuseStrategy.shouldReuseRoute = () => false; }

  //get survey name from url
  getName() {
    let surveyName = this.route.snapshot.paramMap.get('name');
    return (surveyName);
  };

  //get question number from url
  getid() {
    let qnsNum = this.route.snapshot.paramMap.get('id');
    return (qnsNum);
  };

  onSubmit(num: any, question: any) {

    this.surveyName = this.getName();
    this.form.questionNum = num;
    this.qns = question;

    let myFormData = new FormData();
    myFormData.append('qns', this.qns);
    myFormData.append('questionNum', this.form.questionNum);
    myFormData.append('choice', this.form.choice);
    myFormData.append('survey', this.surveyName);

    this.Jarwis.process(myFormData).subscribe(
      success => this.handleSuccess(success),
      error => this.handleError(error)
    );
  }

  //error handling
  handleError(error: any) {
    this.error = error.error.error;
  }

  handleSuccess(success: any) {
    this.success = success.message;
  }

  ngOnInit(){

    this.surveyName = this.getName();

    this.next = this.getid();

    //get the question and answers
    this.Jarwis.getQuestion({ "next": this.next, "survey": this.surveyName }).subscribe((response: any) => {
     this.question = response['questions'];
     if (response['questions'] === null){
       this.done = 'Thank you for completing our survey';
     }
    });
  }

}
